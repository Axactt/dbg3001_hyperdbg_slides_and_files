#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>

#pragma comment(lib, "ws2_32.lib")

int test() {
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct sockaddr_in server;
    const char* http_request = "GET / HTTP/1.1\r\nHost: 192.168.40.1\r\n\r\n";
    char recvbuf[1024];
    int recvbuflen = 1024;
    int iResult;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    // Create a socket
    ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (ConnectSocket == INVALID_SOCKET) {
        printf("socket failed with error: %ld\n", WSAGetLastError());
        WSACleanup();
        return 1;
    }

    // Set up the server address
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("192.168.40.1"); // IP address 
    server.sin_port = htons(80); // HTTP port number

    // Connect to the server
    iResult = connect(ConnectSocket, (struct sockaddr*)&server, sizeof(server));
    if (iResult == SOCKET_ERROR) {
        printf("connect failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

    // Send an HTTP request
    DebugBreak(); // 0xcc

    iResult = send(ConnectSocket, http_request, (int)strlen(http_request), 0);
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }


    // Receive the response
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    if (iResult > 0) {
        recvbuf[iResult] = '\0';
        printf("%s\n", recvbuf);
    }
    else if (iResult == 0) {
        printf("Connection closed\n");
    }
    else {
        printf("recv failed with error: %d\n", WSAGetLastError());
    }

    // Clean up
    closesocket(ConnectSocket);
    WSACleanup();
    return 0;
}


int main() {

    test();
    return test();
}