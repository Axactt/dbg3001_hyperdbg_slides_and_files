If we want to 'call' a function:
rcx,
rdx,
r8,
r9,
rsp+20
rsp+28
rsp+30
and so on.

=======================================

If the 'call' is executed:

rcx,
rdx,
r8,
r9,
rsp+28
rsp+30
rsp+38
and so on.