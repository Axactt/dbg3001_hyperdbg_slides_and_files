//
//
// List of MSRs: https://github.com/gz/rust-x86/blob/master/src/msr.rs
//

/*

 NT (Windows) specific exception vectors (Deciaml)
 
    APC_INTERRUPT   = 31
    DPC_INTERRUPT   = 47
    CLOCK_INTERRUPT = 209
    IPI_INTERRUPT   = 225
    PMI_INTERRUPT   = 254
*/

!exception 0xe script { 
	   printf("page-fault in process : %s, pid: %x, addr: %llx\n", $pname, $pid, @cr2);
}

!interrupt d1 script {
	printf("core: %x, rip: %llx, %x\n", $core, @rip, $pid );
}

!msrread script {
	printf("rip: %llx tries to read: %x MSR\n", @rip, @ecx);
}



!msrwrite script {
	printf("rip: %llx tries to write: %x MSR\n", @rip, @ecx);
}


/*

Read:
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ab90d4b tries to read: 400000f0 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR
	rip: fffff8003ac28eb8 tries to read: c0000100 MSR

Write: 
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ab464b9 tries to write: 830 MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac2916d tries to write: c0000100 MSR
	rip: fffff8003ac2916d tries to write: c0000100 MSR
	rip: fffff8003ac2916d tries to write: c0000100 MSR
	rip: fffff8003ac2916d tries to write: c0000100 MSR
	rip: fffff8003ac2916d tries to write: c0000100 MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ab464cb tries to write: 83f MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
	rip: fffff8003ac1cef9 tries to write: 80b MSR
*/

//
// Process A
//

!interrupt d1 pid 0x9c4 script {

	if (@rip & 0xff000000`00000000) {
		printf("clk interrupt received at the kernel: %llx\n", @rip);
	}
	else{
		pause();
	}
}

0: kHyperDbg> r


r rax=0000000064e856c2 
r rbx=0000000000000000 
r rcx=000000000093f950
r rdx=0000000000000000 
r rsi=0000000000d286cb 
r rdi=000000000093fc50
r rip=0000000000d309cb 
r rsp=000000000093fb60 
r rbp=000000000093fc50

R8=000000000000002b  R9=00000000773c657c  R10=0000000000000000
R11=0000000000000246 R12=0000000000255000 R13=000000000083fda0
R14=000000000083ece0 R15=0000000077344770 IOPL=00
OF 0  DF 0  IF 1  SF  0
ZF 0  PF 1  CF 1  AXF 1
CS 0023 SS 002b DS 002b ES 002b FS 0053 GS 002b
RFLAGS=0000000000000217

0: kHyperDbg> !pte rip
VA d309cb
PML4E (PXE) at ffffdf6fb7dbe000 contains 0a00000156e06867
PDPT (PPE) at ffffdf6fb7c00000  contains 0a00000159d07867
PDE at ffffdf6f80000030 contains 0a0000014f508867
PTE at ffffdf0000006980 contains 0100000147106025


eq ffffdf0000006980 01000001f3bb0867

eq ffffdf0000006980 0100000147106025



////////////////////////////////////////////////////////////////

//
// Process B
//

!interrupt d1 pid 0x2748 script {

	if (@rip & 0xff000000`00000000) {
		printf("clk interrupt received at the kernel: %llx\n", @rip);
	}
	else{
		pause();
	}
}


0: kHyperDbg> !pte 15000c2
VA 15000c2
PML4E (PXE) at ffffdf6fb7dbe000 contains 0a0000014571e867
PDPT (PPE) at ffffdf6fb7c00000  contains 0a000001b491f867
PDE at ffffdf6f80000050 contains 0a0000014544c867
PTE at ffffdf000000a800 contains 01000001f3bb0867


Shell code is located at: 0x1500000