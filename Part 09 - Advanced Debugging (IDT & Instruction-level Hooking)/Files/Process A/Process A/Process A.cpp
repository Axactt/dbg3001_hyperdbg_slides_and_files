#include <Windows.h>
#include <conio.h>
#include <iostream>
#include <time.h>

DWORD WINAPI DoStuff(LPVOID lpParameter)
{
    int index = 0;

    //
    // The new thread will start here
    //
    int i = 0;
    for (;;)
    {
        for (size_t j = 0; j < 0xffffffff; j++)
        {
            i = j;
        }

        printf("thread in the target function | index: %d\n", index);
        index++;

        i++;

    }

    return 0;
}

int main()
{

    printf("process id: 0x%x\n", GetCurrentProcessId());

    //
    // Create a new thread which will start at the DoStuff function
    //
    HANDLE hThread = CreateThread(
        NULL,    // Thread attributes
        0,       // Stack size (0 = use default)
        DoStuff, // Thread start address
        NULL,    // Parameter to pass to the thread
        0,       // Creation flags
        NULL);   // Thread id

    if (hThread == NULL)
    {
        //
        // Thread creation failed.
        // More details can be retrieved by calling GetLastError()
        //
        return 1;
    }

    //
    // Wait for thread to finish execution
    //
    WaitForSingleObject(hThread, INFINITE);

    //
    // Thread handle must be closed when no longer needed
    //
    CloseHandle(hThread);

    _getch();

    return 0;
}